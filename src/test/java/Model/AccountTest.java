package Model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AccountTest {

	
	private Account account ;
	
	@Before
	public void before(){
		account = new Account("Jade Curtiss");
	}
	
	
	@Test
	public void depositTest() {
		account.deposit(42);		
		assertEquals(42,account.getAmount());
		assertEquals(1, account.getStatement().size());
		assertEquals(BalanceType.DEPOSIT, account.getStatement().get(0).getBalance());
		assertEquals(42, account.getStatement().get(0).getAmount());
	}
	
	
	@Test
	public void withdrawalTest() {
		account.withdrawal(42);
		assertEquals(-42,account.getAmount());
		assertEquals(1, account.getStatement().size());
		assertEquals(BalanceType.WITHDRAWAL, account.getStatement().get(0).getBalance());
		assertEquals(42, account.getStatement().get(0).getAmount());
	}
	
}
