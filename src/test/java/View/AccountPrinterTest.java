package View;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import Model.Account;

public class AccountPrinterTest {
	
	@Test
	public void testPrintEmptyAccount() {
				
		String res = AccountPrinter.print(new Account("Milla Maxwell"));
		
		String[] split = res.split("\n");
		assertEquals(3, split.length);
		assertEquals("Milla Maxwell's account", split[0]);
		assertEquals("Operation :", split[1]);
		assertEquals("Amount : 0", split[2]);
	}

	
	@Test
	public void testPrint2Deposit() {				
		Account account = new Account("Milla Maxwell");
		account.deposit(50);
		account.deposit(200);
		
		String res = AccountPrinter.print(account);
		
		String[] split = res.split("\n");
		assertEquals(5, split.length);
		assertEquals("Milla Maxwell's account", split[0]);
		assertEquals("Operation :", split[1]);
		assertEquals("   "+LocalDate.now()+" |  50 | DEPOSIT", split[2]);
		assertEquals("   "+LocalDate.now()+" | 200 | DEPOSIT", split[3]);
		assertEquals("Amount : 250", split[4]);
	}

	@Test
	public void testPrintDepositAndWithdrawal() {				
		Account account = new Account("Milla Maxwell");
		account.deposit(200);
		account.withdrawal(100);
		
		String res = AccountPrinter.print(account);
		
		String[] split = res.split("\n");
		assertEquals(5, split.length);
		assertEquals("Milla Maxwell's account", split[0]);
		assertEquals("Operation :", split[1]);
		assertEquals("   "+LocalDate.now()+" | 200 | DEPOSIT", split[2]);
		assertEquals("   "+LocalDate.now()+" | 100 | WITHDRAWAL", split[3]);
		assertEquals("Amount : 100", split[4]);
	}
	
	
}
