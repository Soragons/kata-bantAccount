package Model;

import java.time.LocalDate;
import java.util.Date;

/**
 * classe représentant une opération
 */

public class AccountStatement {
	
	private LocalDate  date;
	private int amount;
	private BalanceType balance;
	
	
	public AccountStatement(int amount,BalanceType balance) {
		this.date = LocalDate.now();
		this.amount= amount;
		this.balance = balance;
	}
	
	public LocalDate getDate() {
		return date;
	}
	
	public int getAmount() {
		return amount;
	}

	public BalanceType getBalance() {
		return balance;
	}
}
