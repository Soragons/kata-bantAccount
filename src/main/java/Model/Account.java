package Model;

import java.util.List;
import java.util.ArrayList;

/**
 * classe r�presentant un compte
 */

public class Account {

	private String owner ;

	//montant actuel du compte
	private int amount;  
	//liste des op�rations effectu�
	private List<AccountStatement> statement = new ArrayList();
	
	
	public Account(String name) {
		this.owner = name;
		amount = 0 ;	
	}
	
	// d�pot sur le compte
	public void deposit(int amount) {
		this.amount += amount;
		statement.add(new AccountStatement(amount,BalanceType.DEPOSIT));
	}

	// pr�levement sur le compte
	public void withdrawal (int amount) {
		this.amount -= amount;
		statement.add(new AccountStatement(amount,BalanceType.WITHDRAWAL));
	}
	
	public String getOwner() {
		return owner;
	}

	public int getAmount() {
		return amount;
	}

	public List<AccountStatement> getStatement() {
		return statement;
	}
}
