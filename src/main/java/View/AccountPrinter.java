package View;

import java.rmi.StubNotFoundException;

import javax.swing.text.DateFormatter;

import Model.Account;
import Model.AccountStatement;

/**
 * classe qui affiche un compte sous forme d'un string 
 *
 */

public class AccountPrinter {

	public static String print(Account account ) {
		StringBuilder buffer  = new StringBuilder();
		
		buffer.append(account.getOwner()).append("'s account\n");
		buffer.append("Operation :\n");
		
		for(AccountStatement statement : account.getStatement()) {
			buffer.append(String.format("   %s | %3d | %s\n", statement.getDate(),statement.getAmount(),statement.getBalance()));			
		}
		
		buffer.append("Amount : ").append(account.getAmount()).append("\n");		
		
		return buffer.toString();
	}
	
}
