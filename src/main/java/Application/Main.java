package Application;

import Model.Account;
import View.AccountPrinter;

public class Main {

	public static void main(String[] args) {
		
		Account account = new Account("Lloyd Irving");
		account.deposit(100);
		account.deposit(150);
		account.withdrawal(80);
		account.deposit(10);
		account.withdrawal(70);
		account.deposit(333);
		account.withdrawal(10);		
		
		System.out.println(AccountPrinter.print(account));
		
	}
	
}
